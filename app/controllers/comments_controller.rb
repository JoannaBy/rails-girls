class CommentsController < ApplicationController
  def index
  end

  def show
  end

  def new
    @comment = Comment.new
  end

  def create
    comment_params = params["comment"].permit("content", "author", "email", "entry_id")
    comment = Comment.create(comment_params)
    redirect_to(entry_path(params["comment"]["entry_id"]))
  end

  def edit
    @comment = Comment.find(params["id"])
  end

  def update
    comment_params = params["comment"].permit("content", "author", "email", "entry_id")
    comment = Comment.find(params["id"])
    comment.update(comment_params)
    redirect_to(entry_path(params["comment"]["entry_id"]))
  end

  def destroy
    comment = Comment.find(params["id"])
    comment.destroy
    redirect_to(entries_path)
  end
end
