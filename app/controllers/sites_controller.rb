class SitesController < ApplicationController
  def index
  	@sites = Site.all
  end

  def new
  	@site = Site.new
  end

  def create
  	site_params = params["site"].permit("name", "url")
  	site = Site.create(site_params)
  	redirect_to(sites_path)
  end
  
end
